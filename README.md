# sdk-volcengine

SDK-火山引擎。

- 大模型音频生成
  - [x] 查询音色列表：`TtsList`
  - [x] 查询音色列表(分页)：`TtsBatchList`
  - [x] 声音复刻-训练：`TtsAudioUpload`
  - [x] 声音复刻-状态查询：`TtsStatus`
  - [x] TTS语音合成：`TtsMerge`

注：

* 标准的火山引擎接口，Go语言SDK位于Github：[https://github.com/volcengine/volcengine-go-sdk](https://github.com/volcengine/volcengine-go-sdk)
