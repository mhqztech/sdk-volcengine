package vcesdk

import (
	"fmt"
	"testing"
)

func TestTtsBatchList(t *testing.T) {
	AppID := ""
	AccessKey := ""
	SecretAccessKey := ""
	rsp, err := TtsBatchList(TtsBatchListRequest{
		AppId:           AppID,
		AccessKey:       AccessKey,
		SecretAccessKey: SecretAccessKey,
		PageNumber:      1,
		PageSize:        10,
	})
	if err != nil {
		t.Error(err)
	} else {
		fmt.Printf("%+v\n", rsp)
	}
}
