package vcesdk

// 基础应答信息
type CommonResponse struct {
	StatusCode    int    `json:"StatusCode"`    // 成功:0
	StatusMessage string `json:"StatusMessage"` // 错误信息
}

// API的应答元数据
type ApiMetaResponse struct {
	RequestId string `json:"RequestId"`
	Action    string `json:"Action"`
	Version   string `json:"Version"`
	Service   string `json:"Service"`
	Region    string `json:"Region"`
}
