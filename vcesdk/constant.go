package vcesdk

const (
	// 基地址
	OpenSpeechUrl = "https://openspeech.bytedance.com"
	OpenApiUrl    = "https://open.volcengineapi.com"

	// 语言种类
	LanguageCn = 0 // 中文（默认）
	LanguageEn = 1 // 英文
	LanguageJa = 2 // 日语
	LanguageEs = 3 // 西班牙语
	LanguageId = 4 // 印尼语
	LanguagePt = 5 // 葡萄牙语

	// 音频训练状态
	TtsStatusNotFound = 0
	TtsStatusTraining = 1
	TtsStatusSuccess  = 2
	TtsStatusFailed   = 3
	TtsStatusActive   = 4

	// 音色训练状态
	SpeakerStateUnknown   = "Unknown"   // SpeakerID尚未进行训练
	SpeakerStateTraining  = "Training"  // 声音复刻训练中(长时间处于复刻中状态请联系火山引擎技术人员)
	SpeakerStateSuccess   = "Success"   // 声音复刻训练成功，可以进行TTS合成
	SpeakerStateActive    = "Active"    // 已激活(无法再次训练)
	SpeakerStateExpired   = "Expired"   // 火山控制台实例已过期或账号欠费
	SpeakerStateReclaimed = "Reclaimed" // 火山控制台实例已回收

	// 音色列表，https://www.volcengine.com/docs/6561/1257544
	// 通用场景
	VoiceTypeSkss  = "zh_female_shuangkuaisisi_moon_bigtts"    // 爽快思思/Skye，中文、英文
	VoiceTypeWnah  = "zh_male_wennuanahu_moon_bigtts"          // 温暖阿虎/Alvin，中文、英文
	VoiceTypeSnzx  = "zh_male_shaonianzixin_moon_bigtts"       // 少年梓辛/Brayan，中文、英文
	VoiceTypeJqkyM = "multi_male_jingqiangkanye_moon_bigtts"   // かずね（和音）/Javier or Álvaro，日语、西语
	VoiceTypeSkssM = "multi_female_shuangkuaisisi_moon_bigtts" // はるこ（晴子）/Esmeralda，日语、西语
	VoiceTypeGlyjM = "multi_female_gaolengyujie_moon_bigtts"   // あけみ（朱美），日语
	VoiceTypeWqdsM = "multi_male_wanqudashu_moon_bigtts"       // ひろし（広志）/Roberto，日语、西语
	VoiceTypeLjnfh = "zh_female_linjianvhai_moon_bigtts"       // 邻家女孩，中文
	VoiceTypeYbxs  = "zh_male_yuanboxiaoshu_moon_bigtts"       // 渊博小叔，中文
	VoiceTypeYgqn  = "zh_male_yangguangqingnian_moon_bigtts"   // 阳光青年，中文
	VoiceTypeTmxy  = "zh_female_tianmeixiaoyuan_moon_bigtts"   // 甜美小源，中文
	VoiceTypeQczz  = "zh_female_qingchezizi_moon_bigtts"       // 清澈梓梓，中文
	VoiceTypeJsxm  = "zh_male_jieshuoxiaoming_moon_bigtts"     // 解说小明，中文
	VoiceTypeKljj  = "zh_female_kailangjiejie_moon_bigtts"     // 开朗姐姐，中文
	VoiceTypeLjnmh = "zh_male_linjiananhai_moon_bigtts"        // 邻家男孩，中文
	VoiceTypeTmyy  = "zh_female_tianmeiyueyue_moon_bigtts"     // 甜美悦悦，中文
	VoiceTypeXljt  = "zh_female_xinlingjitang_moon_bigtts"     // 心灵鸡汤，中文
	// 趣味方言
	VoiceTypeJqky = "zh_male_jingqiangkanye_moon_bigtts"    // 京腔侃爷/Harmony，中文、英文
	VoiceTypeWwxh = "zh_female_wanwanxiaohe_moon_bigtts"    // 湾湾小何，中文
	VoiceTypeWqds = "zh_female_wanqudashu_moon_bigtts"      // 湾区大叔，中文
	VoiceTypeDmcm = "zh_female_daimengchuanmei_moon_bigtts" // 呆萌川妹，中文
	VoiceTypeGzdg = "zh_male_guozhoudege_moon_bigtts"       // 广州德哥，中文
	VoiceTypeBjxy = "zh_male_beijingxiaoye_moon_bigtts"     // 北京小爷，中文
	VoiceTypeHyxg = "zh_male_haoyuxiaoge_moon_bigtts"       // 浩宇小哥，中文
	VoiceTypeGxyz = "zh_male_guangxiyuanzhou_moon_bigtts"   // 广西远舟，中文
	VoiceTypeMtje = "zh_female_meituojieer_moon_bigtts"     // 妹坨洁儿，中文
	VoiceTypeYzzx = "zh_male_yuzhouzixuan_moon_bigtts"      // 豫州子轩，中文
	// 角色扮演
	VoiceTypeGlyj = "zh_female_gaolengyujie_moon_bigtts" // 高冷御姐，中文
	VoiceTypeAjbz = "zh_male_aojiaobazong_moon_bigtts"   // 傲娇霸总，中文
	VoiceTypeMlny = "zh_female_meilinvyou_moon_bigtts"   // 魅力女友，中文
	VoiceTypeSybk = "zh_male_shenyeboke_moon_bigtts"     // 深夜播客，中文
	VoiceTypeSjny = "zh_female_sajiaonvyou_moon_bigtts"  // 柔美女友，中文
	VoiceTypeYqvy = "zh_female_yuanqinvyou_moon_bigtts"  // 撒娇学妹，中文
	VoiceTypeBrsn = "ICL_zh_female_bingruoshaonv_tob"    // 病弱少女，中文
	VoiceTypeHpnh = "ICL_zh_female_huoponvhai_tob"       // 活泼女孩，中文
	VoiceTypeHann = "ICL_zh_female_heainainai_tob"       // 和蔼奶奶，中文
	VoiceTypeLjay = "ICL_zh_female_linjuayi_tob"         // 邻居阿姨，中文
	VoiceTypeWrxy = "zh_female_wenrouxiaoya_moon_bigtts" // 温柔小雅，中文
	VoiceTypeDfhr = "zh_male_dongfanghaoran_moon_bigtts" // 东方浩然，中文
)
