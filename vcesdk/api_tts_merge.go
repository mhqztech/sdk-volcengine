package vcesdk

import (
	"github.com/google/uuid"
)

// 声音复刻API-2.0
// TTS-语音合成接口
// https://www.volcengine.com/docs/6561/1305191
// 语音合成API
// https://www.volcengine.com/docs/6561/1305191
func TtsMerge(req TtsMergeRequest) (rsp TtsMergeResponse, err error) {
	// 处理参数
	if len(req.UserId) == 0 {
		req.UserId = "default"
	}
	if req.Rate == 0 {
		req.Rate = 24000
	}
	if len(req.Encoding) == 0 {
		req.Encoding = "pcm"
	}
	if req.CompressionRate == 0 {
		req.CompressionRate = 1
	}
	if req.SpeedRatio == 0 {
		req.SpeedRatio = 1
	}
	if req.SilenceDuration == 0 {
		req.SilenceDuration = 125
	}
	cluster := ""
	if req.IsSystemVoice {
		cluster = "volcano_tts"
	} else {
		cluster = "volcano_icl"
	}
	language := ""
	switch req.Language {
	case LanguageCn:
		language = "cn"
	case LanguageEn:
		language = "en"
	case LanguageJa:
		language = "ja"
	case LanguageEs:
		language = "es"
	case LanguageId:
		language = "id"
	case LanguagePt:
		language = "pt"
	default:
		language = "cn"
	}
	// 请求
	apiUrl := "api/v1/tts"
	headers := map[string]string{
		"Content-Type":  "application/json",
		"Authorization": "Bearer;" + req.AccessToken,
	}
	body := map[string]interface{}{
		"app": map[string]interface{}{
			"appid":   req.AppId,
			"token":   "access_token", // 不生效，可传入任意非空值
			"cluster": cluster,
		},
		"user": map[string]interface{}{
			"uid": req.UserId,
		},
		"audio": map[string]interface{}{
			"voice_type":       req.VoiceType,
			"rate":             req.Rate,
			"encoding":         req.Encoding,
			"compression_rate": req.CompressionRate,
			"speed_ratio":      req.SpeedRatio,
			"language":         language,
		},
		"request": map[string]interface{}{
			"reqid":            uuid.NewString(),
			"text":             req.Text,
			"text_type":        "plain",
			"silence_duration": req.SilenceDuration,
			"operation":        "query", // query(非流式，http只能query) / submit(流式)
		},
	}
	rsp, err = post[TtsMergeResponse](OpenSpeechUrl, apiUrl, headers, body, true)
	if err != nil {
		return
	}
	return
}

type TtsMergeRequest struct {
	AppId           string  // AppID
	AccessToken     string  // 服务的Access Token
	IsSystemVoice   bool    // 是否是系统音色，false表示是复刻音色
	VoiceType       string  // 声音类型，复刻音色使用声音ID(speaker id)
	UserId          string  // 可传入任意非空值，传入值可以通过服务端日志追溯
	Text            string  // 合成语音的文本，长度限制1024字节(UTF-8编码)。复刻音色没有此限制，但是HTTP接口有60s超时限制
	Rate            int     // (可选) 音频采样率，默认为24000，可选8000，16000
	Encoding        string  // (可选) 音频编码格式，支wav/pcm/ogg_opus/mp3，默认为pcm
	CompressionRate int     // (可选) opus格式时编码压缩比，[1, 20]，默认为 1
	SpeedRatio      float32 // (可选) 语速，复刻音色[0.2,3]，系统音色[0.8,2]，默认为1，通常保留一位小数即可
	SilenceDuration int     // (可选) 句尾静音时长，单位为ms，默认为125
	Language        int     // (可选) 语言，参见constant.go/Language
}

type TtsMergeResponse struct {
	ReqID     string `json:"reqid"`     // 请求ID，与传入的参数中reqid一致
	Code      int    `json:"code"`      // 请求状态码，成功3000
	Message   string `json:"message"`   // 请求状态信息
	Sequence  int    `json:"sequence"`  // 音频段序号，负数表示合成完毕
	Data      string `json:"data"`      // 合成音频，返回的音频数据，base64编码
	Operation string `json:"operation"` // 操作，query(非流式，http只能query) / submit(流式)
	Addition  struct {
		Duration string `json:"duration"` // 音频时长，返回音频的长度，单位ms
	} `json:"addition"` // 额外信息
}
