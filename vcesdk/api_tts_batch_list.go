package vcesdk

import (
	"encoding/json"
	"fmt"
	"net/url"
)

// 声音复刻API-2.0
// 分页查询SpeakerID状态-BatchListMegaTTSTrainStatus
// https://www.volcengine.com/docs/6561/1305191
// Region = "cn-north-1"
// Service = "speech_saas_prod"
// Version = "2023-11-07"
func TtsBatchList(req TtsBatchListRequest) (rsp TtsBatchListResponse, err error) {
	query := url.Values{}
	query.Set("Action", "BatchListMegaTTSTrainStatus")
	query.Set("Version", "2023-11-07")
	apiUrl := fmt.Sprintf("/?%s", query.Encode())
	body := map[string]interface{}{
		"AppID": req.AppId,
	}
	if req.PageNumber > 0 && req.PageSize > 0 {
		body["PageNumber"] = req.PageNumber
		body["PageSize"] = req.PageSize
	}
	bodyStr, _ := json.Marshal(body)
	headers := sign(req.AccessKey, req.SecretAccessKey, "cn-north-1", "speech_saas_prod", query, bodyStr)
	rsp, err = post[TtsBatchListResponse](OpenApiUrl, apiUrl, headers, body, true)
	if err != nil {
		return
	}
	return
}

type TtsBatchListRequest struct {
	AppId           string // AppID
	AccessKey       string // 用户的密钥
	SecretAccessKey string // 用户的密钥
	PageNumber      int64  // 页码，从1开始
	PageSize        int64  // 页大小
}

type TtsBatchListResponse struct {
	MetaData ApiMetaResponse `json:"ResponseMetadata"`
	Result   struct {
		List       []TtsBatchListItem `json:"Statuses"`   // 音色列表
		PageNumber int64              `json:"PageNumber"` // 页码
		PageSize   int64              `json:"PageSize"`   // 页大小
		TotalCount int64              `json:"TotalCount"` // 总数量
	} `json:"Result"`
}

type TtsBatchListItem struct {
	SpeakerID              string `json:"SpeakerID"`              // 音色ID
	CreateTime             int64  `json:"CreateTime"`             // 创建时间，单位毫秒
	ExpireTime             int64  `json:"ExpireTime"`             // 过期时间，单位毫秒
	DemoAudio              string `json:"DemoAudio"`              // 样例语音URL链接
	InstanceNO             string `json:"InstanceNO"`             // 火山引擎实例序号
	IsActivatable          bool   `json:"IsActivatable"`          // 是否可激活
	State                  string `json:"State"`                  // 音色状态，参见constant.go/SpeakerState
	Version                string `json:"Version"`                // 音色已训练过的次数
	Alias                  string `json:"Alias"`                  // 别名，和控制台同步
	AvailableTrainingTimes int64  `json:"AvailableTrainingTimes"` // 可用剩余训练次数
}
