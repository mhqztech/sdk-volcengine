package vcesdk

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"net/http"
	"net/url"
	"strings"
	"time"
)

func sign(
	accessKey string,
	secretAccessKey string,
	region string,
	service string,
	query url.Values,
	body []byte,
) map[string]string {
	// 日期信息
	now := time.Now().UTC()
	date := now.Format("20060102T150405Z")
	authDate := date[:8]
	// body信息
	payload := hex.EncodeToString(hashSHA256(body))
	// 规范化后的Query String(CanonicalQueryString)
	queryString := strings.Replace(query.Encode(), "+", "%20", -1)
	// 参与签名的Header(SignedHeaders)
	signedHeaders := []string{"content-type", "host", "x-content-sha256", "x-date"}
	signedHeaderString := strings.Join(signedHeaders, ";")
	// 规范化后的Header(CanonicalHeaders)
	headerString := ""
	headerString = headerString + "content-type:" + "application/json; charset=utf-8" + "\n"
	headerString = headerString + "host:" + strings.TrimPrefix(OpenApiUrl, "https://") + "\n"
	headerString = headerString + "x-content-sha256:" + payload + "\n"
	headerString = headerString + "x-date:" + date + "\n"
	// 构建规范请求字符串(CanonicalRequest)
	canonicalString := strings.Join([]string{
		http.MethodPost,
		"/",
		queryString,
		headerString,
		signedHeaderString,
		payload,
	}, "\n")
	// 信任状(CredentialScope)
	credentialScope := authDate + "/" + region + "/" + service + "/request"
	// 待签名字符串(StringToSign)
	signString := strings.Join([]string{
		"HMAC-SHA256",
		date,
		credentialScope,
		hex.EncodeToString(hashSHA256([]byte(canonicalString))),
	}, "\n")
	// 计算签名密钥
	kDate := hmacSHA256([]byte(secretAccessKey), authDate)
	kRegion := hmacSHA256(kDate, region)
	kService := hmacSHA256(kRegion, service)
	kSigning := hmacSHA256(kService, "request")
	// 构建认证请求头
	signature := hex.EncodeToString(hmacSHA256(kSigning, signString))
	// 构建Authorization
	authorization := "HMAC-SHA256" +
		" Credential=" + accessKey + "/" + credentialScope +
		", SignedHeaders=" + signedHeaderString +
		", Signature=" + signature
	// 返回Header信息
	return map[string]string{
		"Authorization":    authorization,
		"Content-Type":     "application/json; charset=utf-8",
		"X-Content-Sha256": payload,
		"X-Date":           date,
	}
}

// HMAC-SHA256加密算法
func hmacSHA256(key []byte, content string) []byte {
	mac := hmac.New(sha256.New, key)
	mac.Write([]byte(content))
	return mac.Sum(nil)
}

// SHA256算法
func hashSHA256(data []byte) []byte {
	hash := sha256.New()
	_, _ = hash.Write(data)
	return hash.Sum(nil)
}
