package vcesdk

import (
	"encoding/json"
	"fmt"
	"net/url"
)

// 声音复刻API-2.0
// 查询SpeakerID状态-ListMegaTTSTrainStatus
// https://www.volcengine.com/docs/6561/1305191
// Region = "cn-north-1"
// Service = "speech_saas_prod"
// Version = "2023-11-07"
func TtsList(req TtsListRequest) (rsp TtsListResponse, err error) {
	query := url.Values{}
	query.Set("Action", "ListMegaTTSTrainStatus")
	query.Set("Version", "2023-11-07")
	apiUrl := fmt.Sprintf("/?%s", query.Encode())
	body := map[string]interface{}{
		"AppID": req.AppId,
	}
	bodyStr, _ := json.Marshal(body)
	headers := sign(req.AccessKey, req.SecretAccessKey, "cn-north-1", "speech_saas_prod", query, bodyStr)
	rsp, err = post[TtsListResponse](OpenApiUrl, apiUrl, headers, body, true)
	if err != nil {
		return
	}
	return
}

type TtsListRequest struct {
	AppId           string // AppID
	AccessKey       string // 用户的密钥
	SecretAccessKey string // 用户的密钥
}

type TtsListResponse struct {
	MetaData ApiMetaResponse `json:"ResponseMetadata"`
	Result   struct {
		List []TtsListItem `json:"Statuses"` // 音色列表
	} `json:"Result"`
}

type TtsListItem struct {
	SpeakerID              string `json:"SpeakerID"`              // 音色ID
	CreateTime             int64  `json:"CreateTime"`             // 创建时间，单位毫秒
	ExpireTime             int64  `json:"ExpireTime"`             // 过期时间，单位毫秒
	DemoAudio              string `json:"DemoAudio"`              // 样例语音URL链接
	InstanceNO             string `json:"InstanceNO"`             // 火山引擎实例序号
	IsActivatable          bool   `json:"IsActivatable"`          // 是否可激活
	State                  string `json:"State"`                  // 音色状态，参见constant.go/SpeakerState
	Version                string `json:"Version"`                // 音色已训练过的次数
	Alias                  string `json:"Alias"`                  // 别名，和控制台同步
	AvailableTrainingTimes int64  `json:"AvailableTrainingTimes"` // 可用剩余训练次数
}
