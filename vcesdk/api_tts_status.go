package vcesdk

// 声音复刻API-2.0
// 创建音色-状态查询
// https://www.volcengine.com/docs/6561/1305191
func TtsStatus(req TtsStatusRequest) (rsp TtsStatusResponse, err error) {
	apiUrl := "api/v1/mega_tts/status"
	headers := map[string]string{
		"Content-Type":  "application/json",
		"Authorization": "Bearer;" + req.AccessToken,
		"Resource-Id":   "volc.megatts.voiceclone",
	}
	body := map[string]interface{}{
		"appid":      req.AppId,
		"speaker_id": req.SpeakerId,
	}
	rsp, err = post[TtsStatusResponse](OpenSpeechUrl, apiUrl, headers, body, true)
	if err != nil {
		return
	}
	return
}

type TtsStatusRequest struct {
	AppId       string // AppID
	AccessToken string // 服务的Access Token
	SpeakerId   string // 唯一音色代号
}

type TtsStatusResponse struct {
	BaseResp   CommonResponse `json:"BaseResp"`
	SpeakerId  string         `json:"speaker_id"`  // 唯一音色代号
	Status     int            `json:"status"`      // 训练状态，状态为2或4时都可以调用tts，参见constant.go/TtsStatus
	CreateTime int64          `json:"create_time"` // 创建时间，单位毫秒
	Version    string         `json:"version"`     // 训练版本
	DemoAudio  string         `json:"demo_audio"`  // 样例音频URL，Success状态时返回，一小时有效，若需要，请下载后使用
}
