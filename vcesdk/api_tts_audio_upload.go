package vcesdk

// 声音复刻API-2.0
// 创建音色-训练
// https://www.volcengine.com/docs/6561/1305191
func TtsAudioUpload(req TtsAudioUploadRequest) (rsp TtsAudioUploadResponse, err error) {
	apiUrl := "api/v1/mega_tts/audio/upload"
	headers := map[string]string{
		"Content-Type":  "application/json",
		"Authorization": "Bearer;" + req.AccessToken,
		"Resource-Id":   "volc.megatts.voiceclone",
	}
	body := map[string]interface{}{
		"appid":      req.AppId,
		"speaker_id": req.SpeakerId,
		"audios": []map[string]interface{}{
			{
				"audio_bytes":  req.AudioData,
				"audio_format": req.AudioFormat,
			},
		},
		"source":     2,
		"language":   req.Language,
		"model_type": 1, // 默认为0，1为2.0效果，0为1.0效果
	}
	rsp, err = post[TtsAudioUploadResponse](OpenSpeechUrl, apiUrl, headers, body, true)
	if err != nil {
		return
	}
	return
}

type TtsAudioUploadRequest struct {
	AppId       string // AppID
	AccessToken string // 服务的Access Token
	SpeakerId   string // 唯一音色代号
	AudioData   string // 音频文件内容，base64编码
	AudioFormat string // 音频文件格式，支持wav、mp3、ogg、m4a、aac、pcm
	Language    int    // (可选) 语言，参见constant.go/Language
}

type TtsAudioUploadResponse struct {
	BaseResp  CommonResponse `json:"BaseResp"`
	SpeakerId string         `json:"speaker_id"` // 唯一音色代号
}
